package edu.arclasalle.geoclustermapper;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
	
public class ConnectionMapper {

	/*
	 * This class is waiting to be wake up by ...the Geoclustering service ?
	 */
	
	// Members:
	private String jsessionid = "";
	private String urlInsertionModule = "http://94.177.191.106/opteemal_DataInsertionModule/rest/";
	
	public ConnectionMapper() {
	}
	
	public ConnectionMapper(String apiServer) {
		urlInsertionModule = apiServer;
	}

	public void test() {
		
		//testQueues();
		
		login("admin@admin.it", "d033e22ae348aeb5660fc2140aec35850c4da997");
		
		getProjects();
		
		getSocioEconomicData ("12", "0.02", "gas");
		getSocioEconomicData ("12", "0.02", "income");
		getSocioEconomicData ("12", "0.02", "electricity");
		getSocioEconomicData ("12", "0.02", "123124");
		
		//getWeatherData("12", "0.02");
		
	}

	public boolean login (String username, String password) {
		
		try {

			String url = urlInsertionModule + "user_services/login?username=" + username + "&password=" + password;

			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("GET");

			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'GET' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);

			if (responseCode == 200) {
				// getting session id
				for (String cookie : con.getHeaderFields().get("Set-Cookie")) {
					String s = cookie.split(";", 2)[0];
					if (s.startsWith("JSESSIONID")) {
						jsessionid = s;
						break;
					}
				}
				return true;
			}
		} catch (Exception e) {
			System.out.println("ProducerTextMessage:" + e);
		}

		return false;
	}
	
	public boolean getProjects () {
		
		try	{
			
			String url = urlInsertionModule + "idc_services/dim/getprojects";
			
			URL obj = new URL(url);
	
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			
			con.setRequestMethod("GET");
			con.setRequestProperty("Cookie", jsessionid);
		
			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'GET' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);
	
			if(responseCode == 200) {
				
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();

				//print result
				System.out.println(response.toString());
			
				return true;
			}
		}
		catch (Exception e){
			System.out.println("ProducerTextMessage:"+e);
		}
		
		return false;
	}	

	public String getSocioEconomicData (String lat, String lon, String dataset) {
		
		StringBuffer response = new StringBuffer();
		try	{
			
			String url = urlInsertionModule + "geo_services/getsocioeconomicdata?lat=" + lat + "&lon=" + lon + "&dataset=" + dataset;
			
			URL obj = new URL(url);
	
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			
			con.setRequestMethod("GET");
			con.setRequestProperty("Cookie", jsessionid);
		
			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'GET' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);
	
			if(responseCode == 200) {
				BufferedReader in = new BufferedReader(
				        new InputStreamReader(con.getInputStream()));
				String inputLine;
				response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();

				//print result
				System.out.println(response.toString());
			}
		}
		catch (Exception e){
			System.out.println("ProducerTextMessage:"+e);
		}
		
		return response.toString();
	}
	
	public String getWeatherData (String lat, String lon) {
		
		StringBuffer response = new StringBuffer();
		try {
			
			String url = urlInsertionModule + "geo_services/getweatherdata?lat=" + lat + "&lon=" + lon;
			
			URL obj = new URL(url);
	
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			
			con.setRequestMethod("GET");
			con.setRequestProperty("Cookie", jsessionid);
		
			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'GET' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);
	
			if(responseCode == 200) {
				
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();

				//print result
				//System.out.println(response.toString());
			}
		}
		catch (Exception e){
			System.out.println("ProducerTextMessage:"+e);
		}
		
		return response.toString();
	}

	public void uploadRDFDataToContextualRepository(String data, Long alProjectID) {
		String url = urlInsertionModule + "idc_services/context_connector/insertContextualDataFile/" + alProjectID;
		uploadRDFDataToContextualRepositoryByURI(url, data);
		/*
		try {		
			System.out.println("--------------------------------------");
			Model model = ModelFactory.createDefaultModel();
			FileOutputStream loOS;
			loOS = new FileOutputStream("c:\\data\\data_geoc_" + alProjectID + ".ttl" , false);
			model.write(loOS,"TURTLE");
			System.out.println("--------------------------------------");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		*/
	}		
	
	public void uploadRDFDataToContextualRepository(String data) {
		String url = urlInsertionModule + "idc_services/context_connector/insertContextualDataFile";
		uploadRDFDataToContextualRepositoryByURI(url, data);
	}

	private void uploadRDFDataToContextualRepositoryByURI(String asURL, String data) {		
		
		try {
		
			URL obj = new URL(asURL);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			OutputStream outputStream;
    	   
			// add request header:
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/octet-stream");
			String urlParameters = "";
    	   
			con.setDoOutput(true);
			outputStream = con.getOutputStream();
			outputStream.write(data.getBytes());
			outputStream.flush();
	
			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'POST' request to URL : " + asURL);
			System.out.println("Post parameters : " + urlParameters);
			System.out.println("Response Code : " + responseCode);
	
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
	
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
	
			//print result
			System.out.println(response.toString());
		}
		catch (Exception e){
			System.out.println("ProducerTextMessage:"+e);
		}
	}
	
	public String queryConstructContextualData(String query) {

		StringBuffer response = new StringBuffer();
		
		try {
			
			//http://94.177.191.106/opteemal_DataInsertionModule/rest/idc_services/context_connector/getContextualData?sparql_query=select%20?c%20where%20{%20[]%20a%20?c.%20}
			//http://94.177.191.106/opteemal_DataInsertionModule/rest/idc_services/context_connector/getContextualData
			//http://94.177.191.106/opteemal_DataInsertionModule/rest/idc_services//context_connector/getContextualData
			String url = urlInsertionModule + "idc_services/context_connector/getContextualData";
			
			URL obj = new URL(url);
	
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			
			con.setRequestMethod("POST");
			con.setRequestProperty("Cookie", jsessionid);
			//con.setRequestProperty("Content-Type", "text/plain");
		
			con.setDoOutput(true);
			StringBuilder result = new StringBuilder();
		    result.append("sparql_query");
	        result.append("=");
	        //result.append(URLEncoder.encode(query, "UTF-8"));
	        result.append(query);
	        
	        OutputStream outputStream = con.getOutputStream();
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
			
			writer.write(result.toString());
			writer.flush();
			writer.close();
			outputStream.close();
			
			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'POST' request to URL : " + url);
			System.out.println("Query : " + result.toString());
			System.out.println("Response Code : " + responseCode);
	
			if(responseCode == 200) {
				
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();

				//print result
				System.out.println("Response: " + response.toString());
			}
		}
		catch (Exception e){
			System.out.println("ProducerTextMessage:"+e);
		}
		
		return response.toString();
	}

	public String querySelectContextualData(String query) {

		StringBuffer response = new StringBuffer();
		
		try	{
			
			//http://94.177.191.106/opteemal_DataInsertionModule/rest/idc_services/context_connector/getContextualData?sparql_query=select%20?c%20where%20{%20[]%20a%20?c.%20}
			// http://94.177.191.106/opteemal_DataInsertionModule/rest/idc_services/context_connector/getContextualData
			//http://94.177.191.106/opteemal_DataInsertionModule/rest/idc_services//context_connector/getContextualData
			String url = urlInsertionModule + "idc_services/context_connector/getContextualDataSparql";
			
			URL obj = new URL(url);
	
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			
			con.setRequestMethod("POST");
			con.setRequestProperty("Cookie", jsessionid);
			//con.setRequestProperty("Content-Type", "text/plain");
		
			con.setDoOutput(true);
			StringBuilder result = new StringBuilder();
		    result.append("sparql_query");
	        result.append("=");
	        //result.append(URLEncoder.encode(query, "UTF-8"));
	        result.append(query);
	        
	        OutputStream outputStream = con.getOutputStream();
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
			
			writer.write(result.toString());
			writer.flush();
			writer.close();
			outputStream.close();
			
			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'POST' request to URL : " + url);
			System.out.println("Query : " + result.toString());
			System.out.println("Response Code : " + responseCode);
	
			if(responseCode == 200) {
				
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;				

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();

				//print result
				System.out.println("Response: " + response.toString());
			}
		}
		catch (Exception e){
			System.out.println("ProducerTextMessage:"+e);
		}
		
		return response.toString();
	}
}


