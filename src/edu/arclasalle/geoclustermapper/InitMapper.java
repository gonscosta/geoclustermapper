package edu.arclasalle.geoclustermapper;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;

// IMPORTANT: Morph is not compatible with the last version of Jena (do not replace the libraries)

public class InitMapper {
	
	// Members
	private String login;
	private String password;
	
	private String input_weather_mapping;

	private String input_weather_data;
	private String input_income_data;       
	private String input_electricity_data;       
	private String input_gas_data;       
    
	private String output_weather_data;
	private String output_income_data;       
	private String output_electricity_data;       
	private String output_gas_data;    
	
	private String output_type;
	
	// Configuration *****************************************
	private static boolean mbUploadWeatherFileTest = false;
	private static boolean mbLocalFileTest = false;
	
	// Constructor
	public static void main(String[] args) {
		
		// Main method is only used for testing:
		
		if(mbUploadWeatherFileTest) {
			/*
			// Uploading electricity prices:
			try {
				String asApiServer = "http://94.177.191.106/opteemal_DataInsertionModule/rest/";
				ConnectionMapper cm = new ConnectionMapper(asApiServer);
				
				Mapper m = new Mapper();				
				String input_electricity_data = cm.querySelectContextualData("select ?x ?c where { ?x a <http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#Energy_Cost> . } limit 100");		
				PrintWriter out = new PrintWriter("data/data.txt");
				out.println(input_electricity_data);
				out.close();
				
				cm.uploadRDFDataToContextualRepository(m.JSONMapper(input_electricity_data, "ELECTRICITY", 72L), 72L);
			} catch (Exception e1) {
				System.out.println(e1);
			}	
			*/
			// *******************************************************************************
		    // LOCAL TESTING (-> see GeoClusterService Java project for OnLine use)
			try {
				String input_weather_data = getWeatherData("F:\\WP2\\work\\tools\\Mapper\\GeoClusterService\\src\\arc\\lasalle\\geoclusterservice\\properties\\ESP_Barcelona.081810_IWEC.epw");
				String input_weather_uri = "https://www.energyplus.net/weather-download/europe_wmo_region_6/ESP//ESP_Barcelona.081810_IWEC/ESP_Barcelona.081810_IWEC.epw";
				String asApiServer = "http://94.177.191.106/opteemal_DataInsertionModule/rest/";
				ConnectionMapper cm = new ConnectionMapper(asApiServer);
				Mapper m = new Mapper();
				String lsRDFModel = m.EPWMapper(input_weather_data, input_weather_uri, "data");
				cm.uploadRDFDataToContextualRepository(lsRDFModel);
			} catch (Exception e1) {
				System.out.println(e1);
			}	
			
		} else if(mbLocalFileTest) {

			String archivo_config = "./data/configuration.properties";
			
			InitMapper init = new InitMapper();
			init.readPropertiesFile(archivo_config);		
			
			Mapper m = new Mapper();
			
			ConnectionMapper cm = new ConnectionMapper();
			
			//cm.test();
			
			cm.login(init.login, init.password);
				
			String ret = cm.queryConstructContextualData("construct { ?x a ?c } where { ?x a ?c. } limit 10");
			
			Model model = ModelFactory.createDefaultModel() ;
	
			try( final InputStream in = new ByteArrayInputStream(ret.getBytes("UTF-8")) ) {
			    // Naturally, you'd substitute the syntax of your actual
			    // content here rather than use N-TRIPLE.		    
			    model.read(in, null, "RDF/XML");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			/*
			//model.read(ret, "RDF/XML");
			
			System.out.println("return: ");
			System.out.println(model);
	
			ret = cm.querySelectContextualData("select ?x ?c where { ?x a ?c. } limit 10");		
			
			JSONObject obj = new JSONObject(ret);
			
			JSONArray vars = obj.getJSONObject("head").getJSONArray("vars");
			JSONArray results = obj.getJSONObject("results").getJSONArray("bindings");
	
			// print information included in the JSon retrieved from the query:
			ArrayList<String> laVars = new ArrayList<String>();
			for (int i = 0; i < vars.length(); i++) {
				String v = vars.getString(i);
				laVars.add(v.toString());
			}
			System.out.println("\n");
			for (int j = 0; j < results.length(); j++) {
				String lsResultLine = "";
				for (int i = 0; i < vars.length(); i++) {
					JSONObject o = results.getJSONObject(j).getJSONObject(laVars.get(i));	
					if(o.getString("type").equals("uri")) {
						lsResultLine += " <" + o.getString("value") + ">";	
					} else {
						lsResultLine += " " + o.getString("value");
					}
				}
				System.out.println(lsResultLine);
			}		
			
			//System.out.println("\nreturn Select: ");
			//System.out.println(ret);
					
			// upload information into the Context Repository: 
			cm.uploadRDFDataToContextualRepository(m.JSONMapper(cm.getSocioEconomicData("12",  "0.02", "gas")));
			cm.uploadRDFDataToContextualRepository(m.JSONMapper(cm.getSocioEconomicData("12",  "0.02", "electricity")));
			cm.uploadRDFDataToContextualRepository(m.JSONMapper(cm.getSocioEconomicData("12",  "0.02", "income")));
	
			// Retrieve the weather data file
			// 1. Get the latitude and longitude from a JSon ????
			// TODO
			// 2. Download the file from the service 
	
			
			writeFile(cm.getWeatherData("12",  "0.02"), "data\\input_weather_data.epw");		
	*/		
			//String jSon = cm.getSocioEconomicData ("12", "0.02", "gas");
	
			//m.JSONMapper(jSon);
			
			//cm.uploadRDFDataToContextualRepository(readFile("data\\nrg_pc_202.rdf", Charset.defaultCharset()));
			
			//writeFile(cm.getWeatherData("12",  "0.02"), "data\\input_weather_data.epw");
			
			//cm.uploadRDFDataToContextualRepository(m.EPWMapper(cm.getWeatherData("12",  "0.02"), "data/Weather.r2rml"));
			
			/*
			System.out.println("Geo-Clustering Mapper");        	
	    	System.out.println("================================================================================================");
	    	System.out.println("Configuration parameters");
	    	System.out.println();
	    	System.out.println("Input weather file    : " + init.input_weather_data);
	    	System.out.println("Input mapping file    : " + init.input_weather_mapping);
	    	System.out.println("Output weather file   : " + init.output_weather_data);
	    	System.out.println();
	    	System.out.println("Input income file     : " + init.input_income_data);
	    	System.out.println("Output income file    : " + init.output_income_data);
	    	System.out.println();
	    	System.out.println("Input eletricity file : " + init.input_electricity_data);
	    	System.out.println("Output eletricity file: " + init.output_electricity_data);
	    	System.out.println();
	    	System.out.println("Input gas file        : " + init.input_gas_data);
	    	System.out.println("Output gas file       : " + init.output_gas_data);
	    	System.out.println();
	    	System.out.println("Output type           : " + init.output_type);
	    	System.out.println("================================================================================================");
	    	System.out.println();
	    	/*
	    	if(!init.input_weather_data.isEmpty() && !init.input_weather_mapping.isEmpty() && !init.output_weather_data.isEmpty())
	    		m.EPWMapper(init.input_weather_data, init.input_weather_mapping, init.output_weather_data, init.output_type);
	    	
	    	if(!init.input_income_data.isEmpty() && !init.output_income_data.isEmpty())
	    		m.JSONMapperFromFile(init.input_income_data, init.output_income_data, init.output_type);
	
	    	if(!init.input_electricity_data.isEmpty() && !init.output_electricity_data.isEmpty())
	    		m.JSONMapperFromFile(init.input_electricity_data, init.output_electricity_data, init.output_type);
	    	
	    	if(!init.input_gas_data.isEmpty() && !init.output_gas_data.isEmpty())
	    		m.JSONMapperFromFile(init.input_gas_data, init.output_gas_data, init.output_type);
	    	*/
	    	
	    	/*if(init.input_type.contentEquals("EPW")) {
				System.out.println("EPW");			
				m.EPWMapper(init.input_data, init.mapping_data, init.output_data, init.output_type);
	        }else{
	        	m.JSONMapper(init.input_data, init.mapping_data, init.output_data, init.output_type);
	        }
	        */		
		}
	}
	
	private void readPropertiesFile( String archivo_config ) {
		 
        Properties prop = new Properties();

        try {
            prop.load( new FileInputStream( archivo_config ) );
        } catch ( Exception ex ) {
            System.err.println("Error, properties file not found or malformed.");
            System.err.println("Path: " + archivo_config );
            return;
        }
        
        login = prop.getProperty("login");
        password = prop.getProperty("password");
        
        input_weather_data = prop.getProperty("input.weather.data");
        input_income_data = prop.getProperty("input.income.data");       
        input_electricity_data = prop.getProperty("input.electricity.data");       
        input_gas_data = prop.getProperty("input.gas.data");       
                
        input_weather_mapping = prop.getProperty("input.weather.mapping");
        
        output_weather_data = prop.getProperty("output.weather.data");
      	output_income_data = prop.getProperty("output.income.data");       
        output_electricity_data = prop.getProperty("output.electricity.data");       
        output_gas_data = prop.getProperty("output.gas.data");      
        
        output_type = prop.getProperty("output.lang");   
        if(output_type.isEmpty()) {
        	output_type = "TURTLE";       
        }
	}
	
	static String readFile(String path, Charset encoding) {
		try {
			
			byte[] encoded = Files.readAllBytes(Paths.get(path));
			return new String(encoded, encoding);
			
		} catch (Exception e) {
		}		
		return "";
	}
	
	private static void writeFile(String weatherData, String filepath) {
		try {
			
			File outputMorphFile = new File(filepath);
			BufferedWriter bw = new BufferedWriter(new FileWriter(outputMorphFile));
			bw.write(weatherData);
			bw.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static String getWeatherData(String asPath) {
		try {
			
			byte[] encoded = Files.readAllBytes(Paths.get(asPath));
			return new String(encoded, Charset.defaultCharset());
			
		} catch (Exception e) {
		}		
		return "";
	}
}






