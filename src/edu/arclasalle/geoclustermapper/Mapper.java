package edu.arclasalle.geoclustermapper;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

import org.apache.commons.io.IOUtils;

import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;

import java.util.HashMap;
import java.util.Map;

/*
import be.ugent.mmlab.rml.core.RMLEngine;
import be.ugent.mmlab.rml.core.StdRMLEngine;
import be.ugent.mmlab.rml.mapdochandler.extraction.std.StdRMLMappingFactory;
import be.ugent.mmlab.rml.mapdochandler.retrieval.RMLDocRetrieval;
import be.ugent.mmlab.rml.model.RMLMapping;
*/	
import es.upm.fi.dia.oeg.morph.base.engine.MorphBaseRunner;
import es.upm.fi.dia.oeg.morph.base.engine.MorphBaseRunnerFactory;
import es.upm.fi.dia.oeg.morph.r2rml.rdb.engine.MorphCSVRunnerFactory;

public class Mapper {

	public Mapper () {	
	}

	public String JSONMapper(String content, String jsonType, Long alProjectID) {
		
		System.out.println();
		System.out.println("JSON parsing");
		System.out.println("============");
		System.out.println();
		
		ArrayList<Double> values = new ArrayList<Double>();
		ArrayList<String> dates = new ArrayList<String>();
		
		StringBuilder country = new StringBuilder("");
		StringBuilder units = new StringBuilder("");
		
		parseContentsSimple(jsonType, content, values, dates, country, units);
		
		System.out.println("json type: " + jsonType);
		System.out.println("country: " + country);
		System.out.println("units: " + units);
		System.out.println(values);
		System.out.println(dates);
		
		System.out.println("================================================================================================");
		
		return createRDFwithJson(jsonType, dates, values, country.toString(), units.toString(), alProjectID);
	}
	
	public String EPWMapper(String content, String input_weather_uri, String path) {
		
		System.out.println();
		System.out.println("EPW parsing");
		System.out.println("===========");
		System.out.println();

		// Json to CSV (-> path+"/tempCSV_tmp.csv"):
		createCsvFile(content, path);
		
		System.out.println();
		System.out.println("================================================================================================");		
		
		// CSV to RDF (-> path + "/outputMorph.rdf"):
		return createRDFwithMorph(path, input_weather_uri);
	}
	
	private String createRDFwithJson(	String jsonType, 
										ArrayList<String> dates, 
										ArrayList<Double> values, 
										String country, 
										String units, 
										Long alProjectID) {

		StringBuilder contents = new StringBuilder ("");
		
		try {
		
			createTerritoryTriples(contents, country, alProjectID);
		
			switch(jsonType) {
			case "WEATHER":
				createWeather(contents, values, dates, country, units, alProjectID);
				break;
			case "ELECTRICITY":
				createElectricityPrices(contents, values, dates, country, units, alProjectID);
				break;
			case "GAS":
				createGasPrices(contents, values, dates, country, units, alProjectID);
				break;
			case "OIL":
				createOilPrices(contents, values, dates, country, units, alProjectID);
				break;
			case "BIOMASS":
				createBiomassPrices(contents, values, dates, country, units, alProjectID);
				break;
			case "INCOME":
				createIncomeHousehold(contents, values, dates, country, units, alProjectID);
				break;
			}
			
			System.out.println (contents);

			Model model = ModelFactory.createDefaultModel().read(
							IOUtils.toInputStream(contents.toString(), "UTF-8"), 
							null, 
							"TURTLE");
			
			/////////////////////////////////////////
			// Save the model for checking...
			System.out.println("--------------------------------------");
			//FileOutputStream loOS = new FileOutputStream("c:\\data\\data_geoc.ttl" , true);
			FileOutputStream loOS = new FileOutputStream("c:\\data\\data_geoc_" + jsonType + ".ttl" , false);
			model.write(loOS,"TURTLE");
			System.out.println("--------------------------------------");
			/////////////////////////////////////////
			
			StringWriter out = new StringWriter();
			model.write(out, "RDF/XML");
			
			
			/////////////////////////////////////////
			// Save the model for checking...
			System.out.println("--------------------------------------");
			FileOutputStream loOS2 = new FileOutputStream("c:\\data\\data_geoc_" + jsonType + ".rdf" , false);
			model.write(loOS2,"RDF/XML");
			System.out.println("--------------------------------------");
			
			
			return out.toString();
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "";
	}

	private void createWeather(	StringBuilder contents, 
								ArrayList<Double> values, 
								ArrayList<String> dates, 
								String country, 
								String units, 
								Long alProjectID) {
		// NOT USED	
		// The Mapper is not used to load this info to the context repository
	}
	
	private void createTerritoryTriples(StringBuilder contents, String country, Long alProjectID) {

		String uri = country.replace (" ", "_").toLowerCase();
		
		contents.append("<http://opteemal-project.eu/resource/eurostat/territory/"+uri+"> a <http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#Territory> ; \n" +
						"	<http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#territoryValue> \""+country+"\" ; \n" + 
						"	<http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#hasTerritory_Information> <http://opteemal-project.eu/resource/eurostat/territory_information/"+uri+"> .\n\n"
						);

	}

	private void createIncomeHousehold(	StringBuilder contents, 
										ArrayList<Double> values, 
										ArrayList<String> dates, 
										String country, 
										String units, 
										Long alProjectID) {
	
		String uriCity = country.replace (" ", "_").toLowerCase();
		String uriUnits = units.replace (" ", "_").toLowerCase();
		
		contents.append("<http://opteemal-project.eu/resource/eurostat/territory_information/"+uriCity+"> a <http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#Territory_Information> ;\n"+
						"	<http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#hasPopulation> <http://opteemal-project.eu/resource/eurostat/population/"+uriCity+"> .\n\n"
						);

		// We iterate over all the values
		for(int i = 0; i < values.size(); i++) {
			
			String uriValue = "income_"+dates.get(i) + "_" + values.get(i).toString().replace(".","_").toLowerCase();
			
			contents.append("<http://opteemal-project.eu/resource/eurostat/population/"+uriCity+"> a <http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#Population> ; \n"+
							"	<http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#hasPopulation_Mean_Income> <http://opteemal-project.eu/resource/eurostat/population_mean_income/"+uriValue+"> .\n\n"
							);		
			
			contents.append("<http://opteemal-project.eu/resource/eurostat/population_mean_income/"+uriValue+"> a <http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#Population_Mean_Income> ;\n"+
							"	<http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#hasDuration> <http://opteemal-project.eu/resource/eurostat/duration/"+alProjectID+"/"+dates.get(i)+"> ;\n"+
							"	<http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#hasUnit> <http://opteemal-project.eu/resource/eurostat/unit/"+uriUnits+"> ;\n"+
							"	<http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#population_Mean_IncomeValue> \""+values.get(i)+"\" .\n\n"
							);
			
			contents.append("<http://opteemal-project.eu/resource/eurostat/duration/"+alProjectID+"/"+dates.get(i)+"> a <http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#Duration> ;\n"+
							"	<http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#durationValue> \""+dates.get(i)+"\". \n\n"
							);
		}
		
		contents.append("<http://opteemal-project.eu/resource/eurostat/unit/"+uriUnits+"> a <http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#Standard_Unit> ;\n"+
						"	<http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#hasValue> \""+units+"\" .\n\n"
						);
	}
	
	private void createElectricityPrices(	StringBuilder contents, 
											ArrayList<Double> values,
											ArrayList<String> dates, 
											String country, 
											String units, 
											Long alProjectID) {
		
		String uriCity = country.replace (" ", "_").toLowerCase();
		String uriUnits = units.replace (" ", "_").toLowerCase();		

		//We iterate over all the values
		for(int i = 0; i < values.size(); i++) {
			String uriValue = "electricity_"+dates.get(i) + "_" + values.get(i).toString().replace(".","_").toLowerCase();
			
			contents.append("<http://opteemal-project.eu/resource/eurostat/territory_information/"+uriCity+"> a <http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#Territory_Information> ;\n"+
							"	<http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#hasEnergyCost> <http://opteemal-project.eu/resource/eurostat/energy_cost/"+alProjectID+"/"+uriValue+"> .\n\n"
							);
			
			contents.append("<http://opteemal-project.eu/resource/eurostat/energy_cost/"+alProjectID+"/"+uriValue+"> a <http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#Energy_Cost> ;\n"+
							"	<http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#hasDuration> <http://opteemal-project.eu/resource/eurostat/duration/"+alProjectID+"/"+dates.get(i)+"> ;\n"+
							"	<http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#hasUnit> <http://opteemal-project.eu/resource/eurostat/unit/"+uriUnits+"> ;\n"+
							"	<http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#hasEnergyCarrier> <http://opteemal-project.eu/resource/eurostat/energy_carrier/eletricity> ;\n"+
							"	<http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#energy_CostValue> \""+values.get(i)+"\" .\n\n"
							);
			
			contents.append("<http://opteemal-project.eu/resource/eurostat/duration/"+alProjectID+"/"+dates.get(i)+"> a <http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#Duration> ;\n"+
							"	<http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#durationValue> \""+dates.get(i)+"\". \n\n"
							);
		}
		
		contents.append("<http://opteemal-project.eu/resource/eurostat/energy_carrier/eletricity> a <http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#Electricity> ;\n"+
						"	<http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#energy_CarrierValue> \"Electricity\" .\n\n"
						);
		contents.append("<http://opteemal-project.eu/resource/eurostat/unit/"+uriUnits+"> a <http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#Standard_Unit> ;\n"+
						"	<http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#hasValue> \""+units+"\" .\n\n"
						);
	}

	private void createGasPrices(	StringBuilder contents, 
									ArrayList<Double> values, 
									ArrayList<String> dates, 
									String country, 
									String units,
									Long alProjectID) {
		
		String uriCity = country.replace (" ", "_").toLowerCase();
		String uriUnits = units.replace (" ", "_").toLowerCase();		

		//We iterate over all the values
		for(int i = 0; i < values.size(); i++) {
			String uriValue = "natural_gas_"+dates.get(i) + "_" + values.get(i).toString().replace(".","_").toLowerCase();
			
			contents.append("<http://opteemal-project.eu/resource/eurostat/territory_information/"+uriCity+"> a <http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#Territory_Information> ;\n"+
							"	<http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#hasEnergyCost> <http://opteemal-project.eu/resource/eurostat/energy_cost/"+alProjectID+"/"+uriValue+"> .\n\n"
							);
			
			contents.append("<http://opteemal-project.eu/resource/eurostat/energy_cost/"+alProjectID+"/"+uriValue+"> a <http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#Energy_Cost> ;\n"+
							"	<http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#hasDuration> <http://opteemal-project.eu/resource/eurostat/duration/"+alProjectID+"/"+dates.get(i)+"> ;\n"+
							"	<http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#hasUnit> <http://opteemal-project.eu/resource/eurostat/unit/"+uriUnits+"> ;\n"+
							"	<http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#hasEnergyCarrier> <http://opteemal-project.eu/resource/eurostat/energy_carrier/natural_gas> ;\n"+
							"	<http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#energy_CostValue> \""+values.get(i)+"\" .\n\n"
							);
			
			contents.append("<http://opteemal-project.eu/resource/eurostat/duration/"+alProjectID+"/"+dates.get(i)+"> a <http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#Duration> ;\n"+
							"	<http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#durationValue> \""+dates.get(i)+"\". \n\n"
							);
		}
		
		contents.append("<http://opteemal-project.eu/resource/eurostat/energy_carrier/natural_gas> a <http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#Natural_Gas> ;\n"+
						"	<http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#energy_CarrierValue> \"Natural gas\" .\n\n"
						);
		contents.append("<http://opteemal-project.eu/resource/eurostat/unit/"+uriUnits+"> a <http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#Standard_Unit> ;\n"+
						"	<http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#hasValue> \""+units+"\" .\n\n"
						);
	}
	
	private void createOilPrices(	StringBuilder contents, 
									ArrayList<Double> values, 
									ArrayList<String> dates, 
									String country, 
									String units,
									Long alProjectID) {
		
		String uriCity = country.replace (" ", "_").toLowerCase();
		String uriUnits = units.replace (" ", "_").toLowerCase();		

		//We iterate over all the values
		for(int i = 0; i < values.size(); i++) {
			String uriValue = "oil_"+dates.get(i) + "_" + values.get(i).toString().replace(".","_").toLowerCase();
			
			contents.append("<http://opteemal-project.eu/resource/eurostat/territory_information/"+uriCity+"> a <http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#Territory_Information> ;\n"+
							"	<http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#hasEnergyCost> <http://opteemal-project.eu/resource/eurostat/energy_cost/"+alProjectID+"/"+uriValue+"> .\n\n"
							);
			
			contents.append("<http://opteemal-project.eu/resource/eurostat/energy_cost/"+alProjectID+"/"+uriValue+"> a <http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#Energy_Cost> ;\n"+
							"	<http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#hasDuration> <http://opteemal-project.eu/resource/eurostat/duration/"+alProjectID+"/"+dates.get(i)+"> ;\n"+
							"	<http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#hasUnit> <http://opteemal-project.eu/resource/eurostat/unit/"+uriUnits+"> ;\n"+
							"	<http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#hasEnergyCarrier> <http://opteemal-project.eu/resource/eurostat/energy_carrier/oil> ;\n"+
							"	<http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#energy_CostValue> \""+values.get(i)+"\" .\n\n"
							);
			
			contents.append("<http://opteemal-project.eu/resource/eurostat/duration/"+alProjectID+"/"+dates.get(i)+"> a <http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#Duration> ;\n"+
							"	<http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#durationValue> \""+dates.get(i)+"\". \n\n"
							);
		}
		
		contents.append("<http://opteemal-project.eu/resource/eurostat/energy_carrier/oil> a <http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#Oil> ;\n"+
						"	<http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#energy_CarrierValue> \"Oil\" .\n\n"
						);
		contents.append("<http://opteemal-project.eu/resource/eurostat/unit/"+uriUnits+"> a <http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#Standard_Unit> ;\n"+
						"	<http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#hasValue> \""+units+"\" .\n\n"
						);
	}
	
	private void createBiomassPrices(	StringBuilder contents,
										ArrayList<Double> values,
										ArrayList<String> dates,
										String country, 
										String units,
										Long alProjectID) {
		
		String uriCity = country.replace (" ", "_").toLowerCase();
		String uriUnits = units.replace (" ", "_").toLowerCase();		

		//We iterate over all the values
		for(int i = 0; i < values.size(); i++) {
			String uriValue = "biomass_"+dates.get(i) + "_" + values.get(i).toString().replace(".","_").toLowerCase();
			
			contents.append("<http://opteemal-project.eu/resource/eurostat/territory_information/"+uriCity+"> a <http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#Territory_Information> ;\n"+
							"	<http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#hasEnergyCost> <http://opteemal-project.eu/resource/eurostat/energy_cost/"+alProjectID+"/"+uriValue+"> .\n\n"
							);
			
			contents.append("<http://opteemal-project.eu/resource/eurostat/energy_cost/"+alProjectID+"/"+uriValue+"> a <http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#Energy_Cost> ;\n"+
							"	<http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#hasDuration> <http://opteemal-project.eu/resource/eurostat/duration/"+alProjectID+"/"+dates.get(i)+"> ;\n"+
							"	<http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#hasUnit> <http://opteemal-project.eu/resource/eurostat/unit/"+uriUnits+"> ;\n"+
							"	<http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#hasEnergyCarrier> <http://opteemal-project.eu/resource/eurostat/energy_carrier/biomass> ;\n"+
							"	<http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#energy_CostValue> \""+values.get(i)+"\" .\n\n"
							);
			
			contents.append("<http://opteemal-project.eu/resource/eurostat/duration/"+alProjectID+"/"+dates.get(i)+"> a <http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#Duration> ;\n"+
							"	<http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#durationValue> \""+dates.get(i)+"\". \n\n"
							);
		}
		
		contents.append("<http://opteemal-project.eu/resource/eurostat/energy_carrier/biomass> a <http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#Biomass> ;\n"+
						"	<http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#energy_CarrierValue> \"Biomass\" .\n\n"
						);
		contents.append("<http://opteemal-project.eu/resource/eurostat/unit/"+uriUnits+"> a <http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#Standard_Unit> ;\n"+
						"	<http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl#hasValue> \""+units+"\" .\n\n"
						);
	}	
	
	private int parseFile(String contentFile, ArrayList<Double> aValues, ArrayList<String> aDates, StringBuilder country, StringBuilder units) {
		
		int jsonType = 0;	//1 Income household, 2 Electricity prices, 3 Gas prices
		int inx = -1;
		boolean end = false;
		
		try {
			String contents = readFile(contentFile, Charset.defaultCharset());
			return parseContents(contents, aValues, aDates, country, units);
		} catch (IOException e) {
			e.printStackTrace();
		} 
		
		return jsonType;
	}
	
	private void parseContentsSimple(	String jsonType,
										String asContent,
										ArrayList<Double> aadValuesOut, 
										ArrayList<String> aasDatesOut, 
										StringBuilder asCountryOut, 
										StringBuilder asUnitsOut) {
		int inx = -1;
		int liIndexValue = 0;
		boolean end = false;
		asContent = asContent.replace("{", "{\n").replace("}", "\n}").replace(",", ",\n").replace("[", "]\n").replace("[", "\n]").replace("\r", "").replace("\n\n", "\n");
		Scanner scan = new Scanner(asContent).useDelimiter("\n");		
		String line = "";
		
		Map<Integer, Double> llstData = new HashMap<Integer, Double>();
		
		ArrayList<Integer> llstYears = new ArrayList<Integer>(); 
		ArrayList<Double> llstValuesOut = new ArrayList<Double>();

		while (scan.hasNext()) {
			
		    line = scan.next();
				
			////////////////////////////
			// values
			if(line.contains("\"value\":")) {
				while(!end) {
					line = scan.next();
					if((inx = line.indexOf(":")) > -1) {
						String value = line.substring(inx+2).replace(",", "").replace(" ", "");
						//System.out.println("value: " + value);
						//aadValuesOut.add(Double.parseDouble(value));
						llstValuesOut.add(Double.parseDouble(value));
					}
					if(line.contains("}")) end = true;
				}
			}
			
			////////////////////////////
			// units
			if(line.contains("\"unit\":")) {
				while (!line.contains("\"category\"")) {
					line = scan.next();
				}
				
				while (!line.contains("\"label\"")) {
					line = scan.next();
				}
				
				line = scan.next();
				if((inx = line.indexOf(":")) > -1) {
					switch(jsonType) {
					case "WEATHER":
						asUnitsOut.append("euros_per_liter");
						break;
					case "ELECTRICITY":
						asUnitsOut.append("Kilowatt-hour");
						break;
					case "GAS":
						asUnitsOut.append("Kilowatt-hour");
						break;
					case "OIL":
						asUnitsOut.append("euros_per_liter");
						break;
					case "BIOMASS":
						asUnitsOut.append("euros_Kilogram");
						break;
					default:
						String lsUnits = line.substring(inx+2).replace("\"", "");
						asUnitsOut.append(lsUnits);
						break;
					}
				}
			}
			
			////////////////////////////
			// country
			if(line.contains("\"geo\":")) {
				
				while (!line.contains("\"category\"")) {
					line = scan.next();
				}
				
				while (!line.contains("\"label\"")) {
					line = scan.next();
				}
				
				line = scan.next();
				if((inx = line.indexOf(":")) > -1) {
					asCountryOut.append(line.substring(inx+1).replace("\"", "").replace(" ", ""));
				}
			}
			////////////////////////////
			// time
			if(line.contains("\"time\":")) {
				
				while (!line.contains("\"index\":")) {
					line = scan.next();
				}
				end = false;
				
				while(!end) {
					line = scan.next();
					if((inx = line.indexOf("\":")) > -1) {
						String value = line.substring(1, inx+1).replace("\"", "").replace(" ", "");
						try {
							String lsYear = value.substring(0, 4);
							int liYear = Integer.valueOf(lsYear);
							System.out.println("date: " + lsYear);
							if(!llstYears.contains(liYear)) {
								llstYears.add(liYear);
								llstData.put(Integer.valueOf(liYear), llstValuesOut.get(liIndexValue));
							}
							liIndexValue++;
						}catch(Exception e) {
							System.out.println("Error: parsing year value > " + e);
						}
					}
					if(line.contains("}")) {
						end = true;
					}
				}
			}
		}	
		
		scan.close();
		
		// Sort list:
		ArrayList keys = new ArrayList(llstData.keySet());
		Collections.sort(keys);
		 
		for (Object loKey : keys) {
			Double ldValue = llstData.get(loKey);
			aasDatesOut.add(String.valueOf(loKey)); // ex: 2016 > "2016"
			aadValuesOut.add(ldValue);				// ex: 0.2367
		}
	}
	
	static String readFile(String path, Charset encoding) throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}
	
	/* Common */	
	private String readFile (String file) {
		String contentFile=null;
		
		try {
			
			BufferedReader br = new BufferedReader(new FileReader(file));
			StringBuilder sb = new StringBuilder();
	        String line = br.readLine();

	        while (line != null) {
	            sb.append(line);
	            sb.append("\n");
	            line = br.readLine();
	        }
	        br.close();
	        
	        contentFile= sb.toString();
	        
		} catch (IOException e) {
			e.printStackTrace();
		} 
		return contentFile;
	}
	
	private void copyInputFileMapping(String pathMappingFile, String typeMapper, String path) {
		try {
			
			File file = new File(pathMappingFile);
			File fileCopy;

			if (typeMapper == "EPW") {
				fileCopy = new File(path + "./mappingMorph.ttl");
			} else {
				fileCopy = new File(path + "./mappingJSON.ttl");
			}
			
			BufferedReader br = new BufferedReader(new FileReader(file));
			BufferedWriter bw = new BufferedWriter(new FileWriter(fileCopy)); 
			
			String line;
			String input="";
			
			while ((line = br.readLine()) != null) {
				bw.write(line + "\n");
			}
			
			br.close();			
			bw.close();
		} catch (IOException e) {

			e.printStackTrace();
		}
	}
	
	private void copyFileOutput(String pathOutputFile, String typeMapper) {
		
		try {
			
			File fileOutput;
			if(typeMapper=="EPW") {
				fileOutput = new File("./data/outputMorph.rdf");
			}else{
				fileOutput = new File ("./data/outputJSON.rdf");
			}			
			
			File outputMorphFile = new File (pathOutputFile);		
			
			BufferedReader br = new BufferedReader(new FileReader(fileOutput));
			BufferedWriter bw = new BufferedWriter(new FileWriter(outputMorphFile)); 
			
			String line;
			String input="";
			
			while((line=br.readLine())!=null)
			{
				bw.write(line+"\n");
			}
			
			br.close();			
			bw.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/*--------*/
	
	/* EPW - CSV - MORPH */
	@SuppressWarnings("resource")
	private void createCsvFile(String contentFile, String path) {	
				
		try	{
			
			/*
			// In case the service is sending a Json file instead of a EPW file:
			JSONParser loParser = new JSONParser();
			org.json.simple.JSONObject aoContent = (org.json.simple.JSONObject) loParser.parse(contentFile);			
			org.json.simple.JSONObject loWeather = (org.json.simple.JSONObject) aoContent.get("weather");// .get("data");
			String loData = (String)loWeather.get("data");
			String[] laValues = loData.split(",");
			*/
			
			File fileCsv = new File(path+"/tempCSV.csv");
			fileCsv.createNewFile();	
			
			int numLine = 0;
			
			Scanner scan = new Scanner(contentFile).useDelimiter("\n");
            BufferedWriter bufferedWriter = new BufferedWriter( new FileWriter(fileCsv));
            String headerCSV="DATE,H1,H2,M1,M2,DATASOURCE,DRY_BULB_TEMPERATURE,DEW_POINT_TEMPERATURE,RELATIVE_HUMIDITY,ATMOSPHERIC_PRESSURE,EXTRATERRESTRIAL_HORIZONTAL_RADIATION,EXTRATERRESTRIAL_DIRECT_NORMAL_RADIATION,HORIZONTAL_INFRARED_RADIATION_INTENSITY_FROM_SKY,GLOBAL_HORIZONTAL_RADIATION,DIRECT_NORMAL_RADIATION,DIFFUSE_HORIZONTAL_RADIATION,GLOBAL_HORIZONTAL_ILLUMINANCE,DIRECT_NORMAL_ILLUMINANCE,DIFFUSE_HORIZONTAL_ILLUMINANCE,ZENITH_LUMINANCE,WIND_DIRECTION,WIND_SPEED,TOTAL_SKY_COVER,OPAQUE_SKY_COVER,VISIBILITY,CEILING_HEIGHT,PRESENT_WEATHER_OBSERVATION,PRESENT_WEATHER_CODES,PRECIPITABLE_WATER,AEROSOL_OPTICAL_DEPTH,SNOW_DEPTH,DAYS_SINCE_LAST_SNOW,ALBEDO,LIQUID_PRECIPITATION_DEPTH,LIQUID_PRECIPITATION_QUANTITY";
            bufferedWriter.write(headerCSV+"\n");
            
            while (scan.hasNextLine()) {
				String actLine = scan.nextLine();
				if (numLine > 7) {
					bufferedWriter.write(actLine + "\n");
					numLine++;
				} else {
					numLine++;
				}
			}
			scan.close();
			bufferedWriter.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
				
	private void changeLanguageMorph(String outputTypeFile) {
		
		try {
			
			File file = new File("./data/morph.csv.properties");			 
			BufferedReader br = new BufferedReader(new FileReader(file));
			
			String line;
			String input="";			

			while ((line = br.readLine()) != null) {
				if (line.startsWith("output.rdflanguage")) {
					String[] valueParameters = line.split("=");
					input += line.replace(valueParameters[1], outputTypeFile) + "\n";
				} else {
					input += line + "\n";
				}
			}
			br.close();
			
			BufferedWriter bw = new BufferedWriter(new FileWriter(file));
			bw.write(input);
			bw.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}	

	private String createRDFwithMorph(String path, String input_weather_uri){
		
		String configurationFile = "morph.csv.properties";
		
		// this path is "C:\Program Files (x86)\eclipse\data" when the service is invoked remotely
		//String configurationDirectory = System.getProperty("user.dir") + "\\data";
		String configurationDirectory = "c:\\data";
		
        try {
        	/*
        	MorphBaseRunnerFactory runnerFactory = new MorphCSVRunnerFactory();
        	MorphBaseRunner runner = runnerFactory.createRunner(configurationDirectory, configurationFile); // <- file name specified inside
            runner.run();              
            
            Model model = ModelFactory.createDefaultModel() ;
			model.read(new File(path + "/outputMorph.rdf").toURI().toString(), "RDF/XML");
			*/			
        	
        	Model model = ModelFactory.createDefaultModel();
        	
			String lsBaseUriRes = "http://opteemal-project.eu/resource/";
			String lsBaseUriOnto = "http://opteemal-project.eu/ontology/";
			
			Resource loSubject = model.createResource(lsBaseUriRes + "weather_uri_" + 
					(new Timestamp(System.currentTimeMillis())).toString().replace(" ", "").replace(":","").replace(".",""));
			Property loProperty = model.createProperty(lsBaseUriOnto + "weatherUri");
			Literal loObject = model.createLiteral(input_weather_uri);
			Statement loStatement = model.createStatement(loSubject, loProperty, loObject);
			model.add(loStatement);
			
			StringWriter out = new StringWriter();
			model.write(out, "RDF/XML");
			
            System.out.println("Batch process DONE------\n\n");
            
            /////////////////////////////////////////////////////
            // TESTING: Store the model
            //File f = new File("data\\outputMorph_testing.ttl");
            //FileOutputStream ont2 = new FileOutputStream(f);
            //model.write(ont2, "TURTLE");
            /////////////////////////////////////////////////////
            
            return out.toString();
            
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Batch process FAILED------\n\n");
        }
        
		return "";
	}
		
	/* JSON - RML Parser */	
	private void createJSONFileInput(String contentFile) {
		try {
			
			File fileJSON = new File("./data/jsonInput.json");
			fileJSON.createNewFile();
			
			Scanner scan = new Scanner(contentFile).useDelimiter("\n");
            BufferedWriter bufferedWriter = new BufferedWriter( new FileWriter(fileJSON));
			
            String line;         
                       
            while (scan.hasNextLine()) 
            {
            	String actLine=scan.nextLine();             		
            	bufferedWriter.write( actLine+"\n");
            		             
            }
            scan.close();
            bufferedWriter.close();         
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String mappingLanguageOutputJSON(String outputTypeFile) {
		String outputType = "";
		switch (outputTypeFile) {
		case "TURTLE":
			outputType = "TURTLE";
			break;
		case "RDF/XML":
			outputType = "RDFXML";
			break;
		case "N-TRIPLE":
			outputType = "NTRIPLES";
			break;
		default:
			outputType = "NTRIPLES";
			break;
		}
		return outputType;
	}
	
	private void createRDFwithRMLParser(String outputLanguageType, String outputTypeFile) {
		/*
		StdRMLMappingFactory mappingFactory = new StdRMLMappingFactory();
		String map_doc ="data/mappingJSON.ttl";
		String outputFile ="data/outputJSON.rdf";
		String outputFormat = outputLanguageType;
		String graphName = "";
		Map<String, String> parameters = null;
		String[] exeTriplesMap = null;
		RMLDocRetrieval mapDocRetrieval = new RMLDocRetrieval();
        Repository repository = mapDocRetrieval.getMappingDoc(map_doc, RDFFormat.TURTLE);
        
        if(repository == null){
        	System.out.println("Problem retrieving the RML Mapping Document");
        	System.exit(1);
		}
        
        System.out.println("========================================");
        System.out.println("Extracting the RML Mapping Definitions..");
        System.out.println("========================================");
        RMLMapping mapping = mappingFactory.extractRMLMapping(repository);
        
        RMLEngine engine = new StdRMLEngine(outputFile);
        engine.run(mapping, outputFile, outputFormat, graphName, parameters, exeTriplesMap, null, null, null);
        */
		
        /*
        try {
	        Model model = ModelFactory.createDefaultModel() ;
	        model.read(new File("data/outputMorph.rdf").toURI().toString(), outputTypeFile);
	        
	        FileOutputStream out;
		
			out = new FileOutputStream("./data/outputMorph.rdf");
			model.write(out, outputTypeFile) ;
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  */
	}
	
	/***********************************************************************************/
	/* Obsolete code */
	/***********************************************************************************/
	
	/* Obsolete 
	public String JSONMapper(String content) {
		
		System.out.println();
		System.out.println("JSON parsing");
		System.out.println("============");
		System.out.println();
		
		ArrayList<Double> values = new ArrayList<Double>();
		ArrayList<String> dates = new ArrayList<String>();
		
		int jsonType = 0;
		StringBuilder country = new StringBuilder("");
		StringBuilder units = new StringBuilder("");
		
		jsonType = parseContents(content, values, dates, country, units);
		
		System.out.println("json type: " + jsonType);
		System.out.println("country: " + country);
		System.out.println("units: " + units);
		System.out.println(values);
		System.out.println(dates);
		
		System.out.println("================================================================================================");
		
		return createRDFwithJson(jsonType, dates, values, country.toString(), units.toString());
	}	
	*/
	
	/* Obsolete 
	public void JSONMapper(String content, String pathOutputFile, String outputTypeFile, Long alProjectID) {
		
		System.out.println();
		System.out.println("JSON parsing");
		System.out.println("============");
		System.out.println();
		
		ArrayList<Double> values = new ArrayList<Double>();
		ArrayList<String> dates = new ArrayList<String>();
		
		int jsonType = 0;
		StringBuilder country = new StringBuilder("");
		StringBuilder units = new StringBuilder("");
		
		jsonType = parseContents (content, values, dates, country, units);
		
		System.out.println("json type: " + jsonType);
		System.out.println("country: " + country);
		System.out.println("units: " + units);
		System.out.println(values);
		System.out.println(dates);
		
		createRDFwithJson(pathOutputFile, jsonType, dates, values, country.toString(), units.toString(), outputTypeFile);

		System.out.println("================================================================================================");
		
		/*
		 * old method using RML, it didnt work when the json file has more than one value 
		String content;
		
		//1. check if path inputFile or string contentFile
		File f = new File(contentFile);
		boolean isFile = f.isFile();	
		
		if(isFile)	content=readFile (contentFile);
		else 		content=contentFile;
		
		createJSONFileInput(content);
		copyInputFileMapping(pathMappingFile, "JSON");		
		String outputLanguageType=mappingLanguageOutputJSON(outputTypeFile);
		createRDFwithRMLParser(outputLanguageType, outputTypeFile);
		copyFileOutput(pathOutputFile, "JSON");
		*//*
	}	
	*/
	
	/* Obsolete */
	private int parseContents(String contents, ArrayList<Double> aValues, ArrayList<String> aDates, StringBuilder country, StringBuilder units) {
		int jsonType = 0;	//1 Income household, 2 Electricity prices, 3 Gas prices
		int inx = -1;
		boolean end = false;
		contents = contents.replace("{", "{\n").replace("}", "\n}").replace(",", ",\n").replace("[", "]\n").replace("[", "\n]").replace("\r", "").replace("\n\n", "\n");
		
		Scanner scan = new Scanner(contents).useDelimiter("\n");		
		
		String line = "";

		while (scan.hasNext()) {
			
		    line = scan.next();
			//System.out.println("Line: " + line);
			
		    ////////////////////////////
		    // type of data
			if(line.contains("\"label\":")) {
				if(line.contains("Income of households")) jsonType = 1;
				else if(line.contains("Electricity prices")) jsonType = 2;
				else if(line.contains("Gas prices")) jsonType = 3;
			}
				
			////////////////////////////
			// values
			if(line.contains("\"value\":")) {
				
				while(!end) {
					line = scan.next();
					if((inx = line.indexOf(":")) > -1) {
						String value = line.substring(inx+2).replace(",", "").replace(" ", "");
						//System.out.println("value: " + value);
						aValues.add(Double.parseDouble(value));
					}
					
					if(line.contains("}")) end = true;
				}
			}
			
			////////////////////////////
			// units
			if(line.contains("\"unit\":")) {
				while (!line.contains("\"category\"")) {
					line = scan.next();
				}
				
				while (!line.contains("\"label\"")) {
					line = scan.next();
				}
				
				line = scan.next();
				if((inx = line.indexOf(":")) > -1) {
					units.append(line.substring(inx+2).replace("\"", ""));
				}
			}
			////////////////////////////
			// country
			if(line.contains("\"geo\":")) {
				
				while (!line.contains("\"category\"")) {
					line = scan.next();
				}
				
				while (!line.contains("\"label\"")) {
					line = scan.next();
				}
				
				line = scan.next();
				if((inx = line.indexOf(":")) > -1) {
					country.append(line.substring(inx+1).replace("\"", "").replace(" ", ""));
				}
			}
			////////////////////////////
			// time
			if(line.contains("\"time\":")) {
				
				while (!line.contains("\"index\":")) {
					line = scan.next();
				}
				end = false;
				
				while(!end) {
					line = scan.next();
					if((inx = line.indexOf("\":")) > -1) {
						String value = line.substring(1, inx+1).replace("\"", "").replace(" ", "");
						//System.out.println("date: " + value);
						aDates.add(value);
					}
					
					if(line.contains("}")) end = true;
				}
			}
		}
		//br.close();
		scan.close(); 
		
		return jsonType;
	}
	
	/* Obsolete
	public void JSONMapperFromFile(String contentFile, String pathOutputFile, String outputTypeFile) {
		
		System.out.println();
		System.out.println("JSON parsing");
		System.out.println("============");
		System.out.println();
		
		ArrayList<Double> values = new ArrayList<Double>();
		ArrayList<String> dates = new ArrayList<String>();
		
		int jsonType = 0;
		StringBuilder country = new StringBuilder("");
		StringBuilder units = new StringBuilder("");
		
		jsonType = parseFile (contentFile, values, dates, country, units);
		
		System.out.println("json type: " + jsonType);
		System.out.println("country: " + country);
		System.out.println("units: " + units);
		System.out.println(values);
		System.out.println(dates);
		
		createRDFwithJson(pathOutputFile, jsonType, dates, values, country.toString(), units.toString(), outputTypeFile);

		System.out.println("================================================================================================");
	}
	*/
	
	/* Obsolete 	
	private String createRDFwithJson(int jsonType, ArrayList<String> dates, ArrayList<Double> values, String country, String units) {

		StringBuilder contents = new StringBuilder ("");
		
		createTerritoryTriples(contents, country);
		
		switch(jsonType) {
			case 1: createIncomeHousehold(contents, values, dates, country, units);
					break;
			case 2: createElectricityPrices(contents, values, dates, country, units);
					break;				
			case 3: createGasPrices(contents, values, dates, country, units);
					break;				
		}
		
		System.out.println (contents);
		
		try {

			Model model = ModelFactory.createDefaultModel()
				   .read(IOUtils.toInputStream(contents.toString(), "UTF-8"), null, "TURTLE");
			
			StringWriter out = new StringWriter();
			model.write(out, "RDF/XML");
			return out.toString();
		
			
		} catch (IOException e) {

			e.printStackTrace();
		}
		
		return "";
	}
	*/
	
	/* Obsolete 
	private Model createRDFwithJson(String pathOutputFile, 
									int jsonType, 
									ArrayList<String> dates,
									ArrayList<Double> values,
									String country, 
									String units, 
									String outputTypeFile) {
		
		StringBuilder contents = new StringBuilder ("");
		
		createTerritoryTriples(contents, country);
		
		switch(jsonType) {
			case 1: createIncomeHousehold(contents, values, dates, country, units);
					break;
			case 2: createElectricityPrices(contents, values, dates, country, units);
					break;				
			case 3: createGasPrices(contents, values, dates, country, units);
					break;				
		}
		
		System.out.println (contents);
		
		try {

			BufferedWriter bw = new BufferedWriter(new FileWriter(new File (pathOutputFile))); 
			
			Model model = ModelFactory.createDefaultModel()
				   .read(IOUtils.toInputStream(contents.toString(), "UTF-8"), null, "TURTLE");
			 
			
			model.write(bw, outputTypeFile);
			
			//System.out.println(model.toString());
			
			bw.close();
			
		} catch (IOException e) {

			e.printStackTrace();
		}
		
		return null;
	}
	*/
	
	/* Obsolete
	private void createRDFwithMorph(String outputTypeFile){
		String configurationFile = "morph.csv.properties";
		String configurationDirectory = System.getProperty("user.dir") + "\\data";
			
        try {
        	MorphBaseRunnerFactory runnerFactory = new MorphCSVRunnerFactory();
        	
        	MorphBaseRunner runner = runnerFactory.createRunner(configurationDirectory, configurationFile);
            runner.run();
            
            Model model = ModelFactory.createDefaultModel() ;
            model.read(new File("data/outputMorph.rdf").toURI().toString(), outputTypeFile);
            
            String extension = "ttl";
            
            if(outputTypeFile.compareTo ("RDF/XML") == 0)
            	extension = "rdf";
            
            FileOutputStream out = new FileOutputStream("./data/outputMorph."+extension);

            model.write(out, outputTypeFile) ;

            out.close();
            
            System.out.println("Batch process DONE------\n\n");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Batch process FAILED------\n\n");
          
        }
	}	
	*/
}
